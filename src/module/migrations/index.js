/* eslint-disable no-unused-vars */
import {MigrationBase} from './base';
import {Migration1MigrateTalentTree} from './1-migrate-talent-tree';

export default class Migrations {
  static list = [Migration1MigrateTalentTree];

  static get latestVersion() {
    return Math.max(...this.list.map(Migration => Migration.version));
  }

  static constructAll() {
    return this.list.map(Migration => new Migration());
  }
}
